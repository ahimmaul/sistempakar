<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');
Route::post('/', 'HomeController@store')->name('home.store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resources([
    'users' => 'UserController',
    'parameters' => 'ParameterController',
    'rules' => 'RuleController',
    'explanations' => 'ExplanationController',
    'tools' => 'ToolController'
]);
