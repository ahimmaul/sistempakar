@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    <h2>Ubah Parameter</h2>
    {{ Form::model($parameter, ['route' => ['parameters.update', $parameter], 'method' => 'put']) }}
    {{ Form::bsText('name', 'Nama', null, $errors)}}
    {{ Form::bsText('belief', 'Belief', null, $errors)}}
    {{ Form::bsSelect('group_id', 'Grup', $groups, null, $errors) }}
    {{ Form::bsSubmit() }}
    {{ Form::close() }}
</div>
@endsection
