@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    @include('components.alert')
    <h2>Tabel Parameter</h2>
    <a class="btn btn-primary mb-3" href="{{ route('parameters.create') }}">Buat</a>
    <table class="table table-hover" id="datatable">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Belief</th>
                <th>Grup</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($parameters as $parameter)
            <tr>
                <td>{{ $parameter->name }}</td>
                <td>{{ $parameter->belief }}</td>
                <th>{{ $parameter->group->name }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('parameters.edit', $parameter) }}" class="btn btn-warning"><i
                                class="fas fa-edit"></i></a>
                        <a href="{{ route('parameters.destroy', $parameter) }}" data-method="delete"
                            data-token="{{ csrf_token() }}" class="btn btn-danger destroy"><i
                                class="fas fa-trash"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
