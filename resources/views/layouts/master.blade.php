<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Sistem Pakar Pemilihan Alat Kontrasepsi</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/easyui/easyui.css') }}" rel="stylesheet">
    <link href="{{ asset('css/easyui/dialog.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/iconic/css/material-design-iconic-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
    <!--===========================================================================================-->

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/starter-template.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <style type="text/css">
        .form-item {
            margin: 3px 5px 5px 5px;
            vertical-align: top;
        }

        .form-item-edited-updated {
            margin: 3px 5px 5px 5px;
            vertical-align: top;
        }

        .form-label {
            vertical-align: top;
            display: inline-block;
            width: 120px;
            padding: 2px;
            margin: 0;
        }

        .form-label-wide {
            vertical-align: top;
            display: inline-block;
            width: 140px;
            padding: 2px;
            margin: 0;
        }

        .form-separator {
            vertical-align: top;
            display: inline-block;
            width: 5px;
            padding: 2px;
            margin: 0;
        }

        .form-field {
            vertical-align: top;
            display: inline-block;
            min-width: 250px;
            padding: 2px;
            margin: 0;

        }

        .form-field select,
        .form-field input {
            min-width: 250px;
        }

        .form-field-permission {
            vertical-align: top;
            display: inline-block;
            min-width: 250px;
            padding: 2px;
            margin: 0;
        }

        .form-img {
            width: 75px;
            max-height: 100px;
            border: 1px dashed #000;
        }

        .form-title {
            font-weight: bold;
            font-size: 120%;
            margin-bottom: 5px;
            border-bottom: 1px solid #D3D3D3;
            margin-bottom: 5px;
        }

        .form-subtitle {
            font-weight: bold;
            border-bottom: 1px solid #D3D3D3;
            margin-bottom: 5px;
            margin-top: 5px;
            padding: 1px 1px 4px 1px;
        }

        .form-error {
            margin: 5px;
            color: #FF0000;
        }

        .dropdown-submenu {
            position: relative;
        }

        .dropdown-submenu .dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -1px;
        }

        .tutorial {
            width: 80%;
            /*box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);*/
            /*background-color: #f9f9f9;*/
            max-width: 800px;
        }

        .tutorial .slider {
            width: 100%;
            height: 300px;
            background-color: #F03861;
        }

        .tutorial ul {
            font-size: 0;
            list-style-type: none;
        }

        .tutorial ul li {
            font-family: "Open Sans", sans-serif;
            font-size: 1rem;
            font-weight: 400;
            color: white;
            ;
            display: inline-block;
            padding: 15px;
            position: relative;
        }

        .tutorial ul li ul {
            display: none;
        }

        .tutorial ul li:hover {
            cursor: pointer;
            color: black;
            background-color: rgba(242, 242, 242, 0.8);
        }

        .tutorial ul li:hover ul {
            display: block;
            margin-top: 15px;
            width: 100%;
            left: 0;
            position: absolute;
        }

        .tutorial ul li:hover ul li {
            display: block;
            color: black;
            background-color: rgba(242, 242, 242, 0.8);
        }

        .tutorial ul li:hover ul li span {
            float: right;
            color: #f9f9f9;
            background-color: #F03861;
            padding: 2px 5px;
            text-align: center;
            font-size: .8rem;
            border-radius: 3px;
        }

        .tutorial ul li:hover ul li:hover {
            background-color: #e0e0e0;
            color: #1b913b;
        }

        .tutorial ul li:hover ul li:hover span {
            background-color: #ee204e;
        }
    </style>
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.6/datatables.min.css" />
    <link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
    @yield('style')
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="{{ route('home.index') }}"><img src="{{ asset('images/kb.jpg') }}" width='100'
                height='50' /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="tutorial">
            <ul>
                <a href="{{ route('home.index') }}">
                    <li>Home</li>
                </a>
                @auth
                <li>Administrasi <i class="fa fa-angle-down"></i>
                    <ul>
                        <a href="{{ route('users.index') }}">
                            <li>User</li>
                        </a>
                        <a href="{{ route('parameters.index') }}">
                            <li>Parameter</li>
                        </a>
                        <a href="{{ route('rules.index') }}">
                            <li>Aturan</li>
                        </a>
                        <a href="{{ route('tools.index') }}">
                            <li>Alat Kontrasepsi</li>
                        </a>
                        <a href="{{ route('explanations.index') }}">
                            <li>Solusi</li>
                        </a>
                    </ul>
                </li>
                @endauth
            </ul>
        </div>
        <div class="tutorial" style="float: right; width: 100%; text-align: right;">
            <ul>
                @guest
                <a href="{{ route('login') }}">
                    <li>Login</li>
                </a>
                @endguest
                @auth
                <li>
                    {{ Form::open(['route' => 'logout']) }}
                    <button type="submit">Logout</button>
                    {{ Form::close() }}
                </li>
                @endauth
            </ul>
        </div>
    </nav>


    <main role="main">
        <div class="starter-template">
            @yield('content')
        </div>

    </main><!-- /.container -->

    <script src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.6/datatables.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.easyui.min.js') }}"></script>
    <script src="{{ asset('js/datagrid-filter.js') }}"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $('.dropdown-menu ul').hide();
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
            $('#back').click(function () {
                window.location.href = "{{ url()->previous() }}";
            })
            $('#datatable').DataTable();
            var laravel = {
                initialize: function () {
                    this.methodLinks = $('a[data-method]');
                    this.token = $('a[data-token]');
                    this.registerEvents();
                },

                registerEvents: function () {
                    this.methodLinks.on('click', this.handleMethod);
                },

                handleMethod: function (e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;

                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ($.inArray(httpMethod, ['PUT', 'DELETE']) === -1) {
                        return;
                    }

                    var conf = false;
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: 'Anda tidak bisa mengembalikan data yang dihapus!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, hapus saja!',
                        cancelButtonText: 'Tidak, biarkan'
                    }).then((result) => {
                        if (result.value) {
                            form = laravel.createForm(link);
                            form.submit();
                        }
                    });

                    e.preventDefault();
                },

                createForm: function (link) {
                    var form =
                        $('<form>', {
                            'method': 'POST',
                            'action': link.attr('href')
                        });

                    var token =
                        $('<input>', {
                            'type': 'hidden',
                            'name': '_token',
                            'value': link.data('token')
                        });

                    var hiddenInput =
                        $('<input>', {
                            'name': '_method',
                            'type': 'hidden',
                            'value': link.data('method')
                        });

                    return form.append(token, hiddenInput)
                        .appendTo('body');
                }
            };

            laravel.initialize();
    } );
    </script>
    @yield('script')
</body>

</html>
