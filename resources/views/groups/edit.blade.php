@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    <h2>Ubah Pengguna</h2>
    {{ Form::model($user, ['route' => ['users.update', $user], 'method' => 'put']) }}
    {{ Form::bsEmail('email', 'Alamat Email', null, $errors)}}
    {{ Form::bsText('name', 'Nama', null, $errors)}}
    {{ Form::bsPassword('password', 'Password', null, $errors) }}
    {{ Form::bsSubmit() }}
    {{ Form::close() }}
</div>
@endsection
