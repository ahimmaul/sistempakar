@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    <form id="form_inferensi" class="login100-form validate-form" method="post"
        data-options="novalidate:false,iframe:false" enctype="multipart/form-data">
        <span class="login100-form-title p-b-37">
            Hasil Konsultasi
        </span>

        <div class="wrap-input100 validate-input m-b-20">
            <div class="panel-heading" style="text-align: center; font-family: SourceSansPro-Bold;
            font-size: 16px;
            color: #4b2354;
            line-height: 1.2;

            display: block;
            width: 100%;
            background: transparent;
            padding: 0 20px 0 23px;">
                Hai, {{ $nama }}
            </div>
            <br />
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="70%">Gejala yang dipilih</th>
                        <th width="30%">Masalah</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <ol>
                                @foreach($parameters as $parameter)
                                <li>{{ $parameter->name }}</li>
                                @endforeach
                            </ol>
                        </td>
                        <td>
                            <ol>
                                @if($result <> '')
                                    <li>{{ $result->name }}</li>
                                    @else
                                    <li>Alat Kontrasepsi tidak ditemukan</li>
                                    @endif
                            </ol>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="70%">Solusi</th>
                        <th width="30%">Tingkat Keyakinan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <ol>
                                @if($result <> '')
                                    @foreach($result->explanations as $explanation)
                                    <li>{{ $explanation->description }}</li>
                                    @endforeach
                                    @else
                                    <li>Solusi tidak ditemukan</li>
                                    @endif
                            </ol>
                        </td>
                        <td>
                            <ol>
                                @if($result->densitas <> '')
                                    <li>{{ $result->densitas }}%</li>
                                    @else
                                    <li>Tingkat Keyakinan tidak ditemukan</li>
                                    @endif
                            </ol>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="text-center">
            <a href="{{ route('home.index') }}" class="txt2 hov1">
                Back
            </a>
        </div>
    </form>
</div>
@endsection
