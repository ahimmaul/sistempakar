@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    @include('components.alert')
    <h2>Tabel Pengguna</h2>
    <a class="btn btn-primary mb-3" href="{{ route('users.create') }}">Buat</a>
    <table class="table table-hover" id="datatable">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Alamat Email</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('users.edit', $user) }}" class="btn btn-warning"><i
                                class="fas fa-edit"></i></a>
                        <a href="{{ route('users.destroy', $user) }}" data-method="delete"
                            data-token="{{ csrf_token() }}" class="btn btn-danger destroy"><i
                                class="fas fa-trash"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
