<div class="btn-group" role="group">
    {{ Form::submit($label, ['class' => 'btn btn-primary']) }}
    {{ Form::button('Kembali', ['class' => 'btn btn-secondary', 'id' => 'back']) }}
</div>
