<div class="mb-3">
    {{ Form::label($name, $label) }}
    {{ Form::textarea($name, $value, array_merge(($errors->has($name) ? ['class' => 'form-control is-invalid'] : ['class' => 'form-control']), $attributes)) }}
    @error($name)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
