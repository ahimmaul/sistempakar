<div class="mb-3">
    {{ Form::label($name, $label) }}
    {{ Form::text($name, $value, array_merge(($errors->has($name) ? ['class' => 'form-control is-invalid', 'required'] : ['class' => 'form-control', 'required']), $attributes)) }}
    @error($name)
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
