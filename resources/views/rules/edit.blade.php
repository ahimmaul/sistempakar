@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    <h2>Ubah Aturan</h2>
    {{ Form::model($rule, ['route' => ['rules.update', $rule], 'method' => 'put']) }}
    {{ Form::bsSelect('tool_id', 'Masalah', $tools, null, $errors) }}
    {{ Form::bsSelect('parameter_id', 'Gejala', $parameters, null, $errors) }}
    {{ Form::bsSubmit() }}
    {{ Form::close() }}
</div>
@endsection
