@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    @include('components.alert')
    <h2>Tabel Aturan</h2>
    <a class="btn btn-primary mb-3" href="{{ route('rules.create') }}">Buat</a>
    <table class="table table-hover" id="datatable">
        <thead>
            <tr>
                <th>#</th>
                <th>Masalah</th>
                <th>Gejala</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rules as $rule)
            <tr>
                <td>#</td>
                <td>{{ $rule->tool->name }}</td>
                <th>{{ $rule->parameter->name }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('rules.edit', $rule) }}" class="btn btn-warning"><i
                                class="fas fa-edit"></i></a>
                        <a href="{{ route('rules.destroy', $rule) }}" data-method="delete"
                            data-token="{{ csrf_token() }}" class="btn btn-danger destroy"><i
                                class="fas fa-trash"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="text-center">
    <a href="#" class="txt2 hov1">
        Back
    </a>
</div>
@endsection
