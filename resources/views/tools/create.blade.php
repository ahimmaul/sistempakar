@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    <h2>Tambah Alat Kontrasepsi</h2>
    {{ Form::open(['route' => 'tools.store', 'files' => true]) }}
    {{ Form::bsText('name', 'Nama', null, $errors)}}
    {{ Form::bsTextarea('description', 'Deskripsi', null, $errors)}}
    {{ Form::bsFile('image', 'Gambar', null, $errors)}}
    {{ Form::bsSubmit() }}
    {{ Form::close() }}
</div>
@endsection
