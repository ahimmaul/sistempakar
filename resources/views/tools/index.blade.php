@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    @include('components.alert')
    <h2>Tabel Alat Kontrasepsi</h2>
    <a class="btn btn-primary mb-3" href="{{ route('tools.create') }}">Buat</a>
    <table class="table table-hover" id="datatable">
        <thead>
            <tr>
                <th>Foto</th>
                <th>Nama</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tools as $tool)
            <tr>
                <td>
                    @isset($tool->image)
                    <img style="width:100px;height:100px" src="{{ asset('storage/'.$tool->image) }}">
                    @endisset
                </td>
                <td>{{ $tool->name }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('tools.edit', $tool) }}" class="btn btn-warning"><i
                                class="fas fa-edit"></i></a>
                        <a href="{{ route('tools.destroy', $tool) }}" data-method="delete"
                            data-token="{{ csrf_token() }}" class="btn btn-danger destroy"><i
                                class="fas fa-trash"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
