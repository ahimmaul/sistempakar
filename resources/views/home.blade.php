@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    {{ Form::open(['route' => 'home.store', 'class' => 'login100-form validate-form', 'id' => 'form_inferensi', 'data-options' => 'novalidate:false,iframe:false']) }}
    <span class="login100-form-title p-b-37">
        Sistem Pakar Pemilihan Alat Kontrasepsi
    </span>

    <div class="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
        <input class="input100" type="text" name="nama_pengguna" placeholder="Nama Anda" required>
        <span class="focus-input100"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-20">
        <div class="panel-heading" style="text-align: center; font-family: SourceSansPro-Bold;
            font-size: 16px;
            color: #4b2354;
            line-height: 1.2;

            display: block;
            width: 100%;
            background: transparent;
            padding: 0 20px 0 23px;">
            Pilih Kondisi:
        </div>

        <ul class="list-group">
            @foreach($groups as $group)
            <li class="list-group-item">
                <div class="mb-3">
                    {{ Form::label('group['.$group->id.']', $group->name) }}
                    {{ Form::select('group['.$group->id.']', $group->selects, null, ['class' => 'form-control', 'required', 'placeholder' => 'Pilih']) }}
                </div>
            </li>
            @endforeach
        </ul>

    </div>

    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Proses
        </button>
    </div>

    <div class="text-center">
        <a href="#" class="txt2 hov1">
            Reset
        </a>
    </div>
    {{ Form::close() }}
</div>
@endsection
