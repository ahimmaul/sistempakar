@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    @include('components.alert')
    <h2>Tabel Solusi</h2>
    <a class="btn btn-primary mb-3" href="{{ route('explanations.create') }}">Buat</a>
    <table class="table table-hover" id="datatable">
        <thead>
            <tr>
                <th>Masalah</th>
                <th>Deskripsi</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($explanations as $explanation)
            <tr>
                <td>{{ $explanation->tool->name }}</td>
                <td>{{ $explanation->description }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('explanations.edit', $explanation) }}" class="btn btn-warning"><i
                                class="fas fa-edit"></i></a>
                        <a href="{{ route('explanations.destroy', $explanation) }}" data-method="delete"
                            data-token="{{ csrf_token() }}" class="btn btn-danger destroy"><i
                                class="fas fa-trash"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
