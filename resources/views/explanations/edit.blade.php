@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    <h2>Ubah Solusi</h2>
    {{ Form::model($explanation, ['route' => ['explanations.update', $explanation], 'method' => 'put']) }}
    {{ Form::bsSelect('tool_id', 'Masalah', $tools, null, $errors)}}
    {{ Form::bsText('description', 'Deskripsi', null, $errors)}}
    {{ Form::bsSubmit() }}
    {{ Form::close() }}
</div>
@endsection
