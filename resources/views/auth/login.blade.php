@extends('layouts.master')

@section('content')
<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
    {{ Form::open(['route' => 'login', 'class' => 'login100-form validate-form']) }}
    <span class="login100-form-title p-b-37">
        Login Admin
    </span>

    <div class="wrap-input100 validate-input m-b-20" data-validate="email">
        <input class="input100" type="text" name="email" placeholder="email">
        <span class="focus-input100"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-25" data-validate="Enter password">
        <input class="input100" type="password" name="password" placeholder="password">
        <span class="focus-input100"></span>
    </div>

    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            LogIn
        </button>
    </div>
    {{ Form::close() }}
</div>
@endsection
