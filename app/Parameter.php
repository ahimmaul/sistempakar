<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $fillable = [
        'name', 'belief', 'group_id'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function tools()
    {
        return $this->hasMany(Tool::class);
    }
}
