<?php

namespace App\Http\Controllers;

use App\Tool;
use App\Explanation;
use Illuminate\Http\Request;

class ExplanationController extends Controller
{
    public function index()
    {
        $explanations = Explanation::all();

        return view('explanations.index', compact('explanations'));
    }

    public function create()
    {
        $tools = Tool::pluck('name', 'id');

        return view('explanations.create', compact('tools'));
    }

    public function store(Request $request)
    {
        Explanation::create($request->all());

        return redirect()->route('explanations.index')->with('success', 'Solusi berhasil dibuat');
    }

    public function edit(Explanation $explanation)
    {
        $tools = Tool::pluck('name', 'id');

        return view('explanations.edit', compact('explanation', 'tools'));
    }

    public function update(Request $request, Explanation $explanation)
    {
        $explanation->fill($request->all());
        $explanation->save();

        return redirect()->route('explanations.index')->with('success', 'Solusi berhasil diubah');
    }

    public function destroy(Explanation $explanation)
    {
        $explanation->delete();

        return redirect()->route('explanations.index')->with('success', 'Solusi berhasil dihapus');
    }
}
