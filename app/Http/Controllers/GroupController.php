<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        $groups = group::all();

        return view('groups.index', compact('groups'));
    }

    public function create()
    {
        return view('groups.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required'
        ]);

        $group = new group;
        $group->name = $request->name;
        $group->email = $request->email;
        $group->email_verified_at = now();
        $group->password = bcrypt($request->password);
        $group->save();

        return redirect()->route('groups.index')->with('success', 'Pengguna berhasil dibuat');
    }

    public function edit(group $group)
    {
        return view('groups.edit', compact('group'));
    }

    public function update(Request $request, group $group)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required'
        ]);

        $group->fill($request->all());
        $group->password = bcrypt($request->password);
        $group->save();

        return redirect()->route('groups.index')->with('success', 'Pengguna berhasil diubah');
    }

    public function destroy(group $group)
    {
        $group->delete();

        return redirect()->route('groups.index')->with('success', 'Pengguna berhasil dihapus');
    }
}
