<?php

namespace App\Http\Controllers;

use App\Tool;
use App\Group;
use App\Parameter;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        foreach ($groups as $group) {
            $group->selects = $group->parameters->pluck('name', 'id');
        }

        return view('home', compact('groups'));
    }

    public function store(Request $request)
    {
        $parameters = Parameter::find($request->group);
        $parameters_id = $parameters->pluck('id')->toArray();
        $groups_count = Group::all()->count();
        $nama = $request->nama_pengguna;
        $result = '';

        // $tools = Tool::withCount(['rules' => function ($query) use ($parameters_id) {
        //     $query->whereIn('parameter_id', $parameters_id);
        // }])->get();
        // foreach ($tools as $tool) {
        //     if ($tool->rules_count == $groups_count) {
        //         $result = $tool;
        //     }
        // }

        $result = Tool::withCount(['rules' => function ($query) use ($parameters_id) {
            $query->whereIn('parameter_id', $parameters_id);
        }])->orderBy('rules_count', 'desc')->first();

        foreach($parameters as $parameter) {
            $parameter->probability = 1 - $parameter->belief;

            if($parameter->belief == 1 || $parameter->belief == 0) {
                $result->densitas = 100;
                // dd($result->densitas);
                break;
            }
        }

        if($result->densitas <> 100) {
            $arr = [];
            $arr1 = [];
            $arr2 = [];
            $arr3 = [];

            $arr[0] = $parameters[0]->belief;
            $arr[1] = $parameters[0]->probability;

            foreach($arr as $data) {
                array_push($arr1, ($data * $parameters[1]->belief)/1, ($data * $parameters[1]->probability)/1);
            }
            foreach($arr1 as $data) {
                array_push($arr2, ($data * $parameters[2]->belief)/1, ($data * $parameters[2]->probability)/1);
            }
            foreach($arr2 as $data) {
                array_push($arr3, ($data * $parameters[3]->belief)/1, ($data * $parameters[3]->probability)/1);
            }
            // dd($arr);
            $result->densitas = max($arr3)*100;
        }

        return view('home.result', compact('result', 'nama', 'parameters'));
    }
}
