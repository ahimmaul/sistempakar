<?php

namespace App\Http\Controllers;

use App\Group;
use App\Parameter;
use Illuminate\Http\Request;

class ParameterController extends Controller
{
    public function index()
    {
        $parameters = Parameter::all();

        return view('parameters.index', compact('parameters'));
    }

    public function create()
    {
        $groups = Group::pluck('name', 'id');

        return view('parameters.create', compact('groups'));
    }

    public function store(Request $request)
    {
        Parameter::create($request->all());

        return redirect()->route('parameters.index')->with('success', 'Parameter berhasil dibuat');
    }

    public function edit(Parameter $parameter)
    {
        $groups = Group::pluck('name', 'id');

        return view('parameters.edit', compact('parameter', 'groups'));
    }

    public function update(Request $request, Parameter $parameter)
    {
        $parameter->fill($request->all());
        $parameter->save();

        return redirect()->route('parameters.index')->with('success', 'Parameter berhasil diubah');
    }

    public function destroy(Parameter $parameter)
    {
        $parameter->delete();

        return redirect()->route('parameters.index')->with('success', 'Parameter berhasil dihapus');
    }
}
