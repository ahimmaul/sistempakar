<?php

namespace App\Http\Controllers;

use App\Tool;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    public function index()
    {
        $tools = Tool::all();

        return view('tools.index', compact('tools'));
    }

    public function create()
    {
        return view('tools.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $tool = new Tool;
        $tool->name = $request->name;
        $tool->description = $request->description;

        if ($request->has('image')) {
            $image = $request->file('image');
            $folder = 'images/';

            $name = $request->user()->id . '_' . time() . '.' . $image->getClientOriginalExtension();
            $request->file('image')->storeAs('public/' . $folder, $name);
            $url = $folder . $name;

            $tool->image = $url;
        }
        $tool->save();

        return redirect()->route('tools.index')->with('success', 'Masalah berhasil dibuat');
    }

    public function edit(Tool $tool)
    {
        return view('tools.edit', compact('tool'));
    }

    public function update(Request $request, Tool $tool)
    {
        $tool->fill($request->all());
        if ($request->has('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg|max:2048'
            ]);

            $image = $request->file('image');
            $folder = 'images/';

            $name = $request->user()->id . '_' . time() . '.' . $image->getClientOriginalExtension();
            $request->file('image')->storeAs('public/' . $folder, $name);
            $url = $folder . $name;

            $tool->image = $url;
        }
        $tool->save();

        return redirect()->route('tools.index')->with('success', 'Masalah berhasil diubah');
    }

    public function destroy(Tool $tool)
    {
        $tool->delete();

        return redirect()->route('tools.index')->with('success', 'Masalah berhasil dihapus');
    }
}
