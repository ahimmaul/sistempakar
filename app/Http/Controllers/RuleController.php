<?php

namespace App\Http\Controllers;

use App\Rule;
use App\Tool;
use App\Parameter;
use Illuminate\Http\Request;

class RuleController extends Controller
{
    public function index()
    {
        $rules = Rule::all();

        return view('rules.index', compact('rules'));
    }

    public function create()
    {
        $parameters = Parameter::pluck('name', 'id');
        $tools = Tool::pluck('name', 'id');

        return view('rules.create', compact('tools', 'parameters'));
    }

    public function store(Request $request)
    {
        Rule::create($request->all());

        return redirect()->route('rules.index')->with('success', 'Aturan berhasil dibuat');
    }

    public function edit(rule $rule)
    {
        $parameters = Parameter::pluck('name', 'id');
        $tools = Tool::pluck('name', 'id');

        return view('rules.edit', compact('rule', 'parameters', 'tools'));
    }

    public function update(Request $request, rule $rule)
    {
        $rule->fill($request->all());
        $rule->save();

        return redirect()->route('rules.index')->with('success', 'Aturan berhasil diubah');
    }

    public function destoy(rule $rule)
    {
        $rule->delete();

        return redirect()->route('rules.index')->with('success', 'Aturan berhasil dihapus');
    }
}
