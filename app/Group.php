<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function parameters()
    {
        return $this->hasMany(Parameter::class);
    }
}
