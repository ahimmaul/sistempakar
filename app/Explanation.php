<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Explanation extends Model
{
    protected $fillable = [
        'tool_id', 'description'
    ];

    public function tool()
    {
        return $this->belongsTo(Tool::class);
    }
}
