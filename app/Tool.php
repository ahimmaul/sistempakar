<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    protected $fillable = [
        'name', 'description', 'image'
    ];

    protected $attributes = [
        'image' => ''
    ];

    public function rules()
    {
        return $this->hasMany(Rule::class);
    }

    public function explanations()
    {
        return $this->hasMany(Explanation::class);
    }
}
