<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $fillable = [
        'tool_id', 'parameter_id'
    ];

    public function tool()
    {
        return $this->belongsTo(Tool::class);
    }

    public function parameter()
    {
        return $this->belongsTo(Parameter::class);
    }
}
