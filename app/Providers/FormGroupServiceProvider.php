<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormGroupServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsText', 'components.text', ['name', 'label', 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsFile', 'components.file', ['name', 'label', 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsEmail', 'components.email', ['name', 'label', 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsPassword', 'components.password', ['name', 'label', 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsNumber', 'components.number', ['name', 'label', 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsTextarea', 'components.textarea', ['name', 'label', 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsSelect', 'components.select', ['name', 'label', 'options' => [], 'value' => null, 'errors' => [], 'attributes' => []]);
        Form::component('bsSubmit', 'components.submit', ['label' => 'Simpan']);
    }
}
